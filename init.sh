#!/bin/bash

# author: timo@codewandler.org

# PI Strap - Bootstrap your PI 

# check for root
if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root" 1>&2
  exit 1
fi

# check for device
if [ $# -lt 2 ]; then
  echo "usage $0 $BOOT $ROOT"
  exit;
fi;

BOOT=$1
ROOT=$2

# TODO check if valid device
# TODO check if this really is a sd card :)

function confirm(){
  echo "$1"
  echo 'confirm with `y`'
  read answer
  if [ $answer != 'y' ]; then
    exit 2
  fi;
}

# test if file exists
if [ ! -e $BOOT ]; then
  echo "devices do not exist: $BOOT"; 
  exit 1;
fi;

# test if file exists
if [ ! -e $ROOT ]; then
  echo "devices do not exist: $ROOT"; 
  exit 1;
fi;

fdisk -l | grep sdc

echo "---------------------"
echo "/    : $ROOT"
echo "/boot: $BOOT"
echo "---------------------"
confirm "Is this correct ($DEV) ?"

# format disks
# todo, do we have to partition it correctly before that ?

# create boot
echo "creating filesystem $BOOT ..."
mkfs.vfat $BOOT
mkdir -p boot
mount $BOOT boot
echo "$BOOT mounted ..."

# create root
echo "creating filesystem $ROOT ..."
mkfs.ext4 $ROOT
mkdir -p root
mount $ROOT root
echo "$ROOT mounted ..."

# download the image if it does not exist
echo "verify image ..."
IMG=ArchLinuxARM-rpi-latest.tar.gz
if [ ! -f $IMG ]; then
  echo "downloading image ..."
  wget http://archlinuxarm.org/os/$IMG
fi;

# extract to pi
echo "extracting image to $ROOT ..."
bsdtar -xpf ArchLinuxARM-rpi-latest.tar.gz -C root

# sync up
echo "finalizing storage ..."
time sync

# apply boot
mv root/boot/* boot

# cleanup
umount boot 
umount root

rm -r boot root

echo "done!"
