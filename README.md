### Raspberry 3 with arch ###

https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3
```
The Raspberry Pi 3 is the successor to the Raspberry Pi 2. It builds upon the Pi 2 by upgrading the ARM cores to Cortex-A53 and adding an onboard single-band 2.4GHz-only wireless chipset.

The Raspberry Pi 3 measures the same 85.60mm x 53.98mm x 17mm, with a little overlap for the SD card and connectors which project over the edges. The SoC is a Broadcom BCM2837. This contains a quad-core Coretx-A53 running at 1.2GHz and a Videocore 4 GPU.
```

### Detect your SD-Card ###
```sh
# make sure card is not plugged
dmesg -w
# plugin the card now and wait for output
```
Output can be something like this:

```
[ 2449.029057]  sdc: sdc1
[ 2467.579370] sd 6:0:0:0: [sdc] 60751872 512-byte logical blocks: (31.1 GB/29.0 GiB)
[ 2467.585841]  sdc: sdc1

```

So I know my SD Card is linked to `sdc` and remember that name ...

### Formatting ###

Use fdisk -l to show which partitions are already present on your card.
Use gparted or fdisk to format your card. In my case the partition table looks like that:

TODO: Image

We need two partitions for the following script to work, in my case these are /dev/sdc1 and /dev/sdc5

The first one will contain the boot area and the second one will contain the system. In my case I have created
an additional data partition.

### Run the script as with sudo ###

```sh
sudo ./init.sh /dev/sdc1 /dev/sdc5
```

Possible Output:

```sh
[timo@machine pistrap]$ sudo ./init.sh /dev/sdc1 /dev/sdc5
Disk /dev/sdc: 29 GiB, 31104958464 bytes, 60751872 sectors
/dev/sdc1           2048  2050047  2048000 1000M  6 FAT16
/dev/sdc2        2050048 60751871 58701824   28G  5 Extended
/dev/sdc5        2052096 28702719 26650624 12.7G 83 Linux
/dev/sdc6       28704768 60751871 32047104 15.3G 83 Linux
---------------------
/    : /dev/sdc5
/boot: /dev/sdc1
---------------------
Is this correct () ?
confirm with `y`
y
creating filesystem /dev/sdc1 ...
mkfs.fat 3.0.28 (2015-05-16)
/dev/sdc1 mounted ...
creating filesystem /dev/sdc5 ...
mke2fs 1.42.13 (17-May-2015)
/dev/sdc5 contains a ext4 file system
	last mounted on /media/data/codewandler/projects/rpi/pistrap/root on Thu Mar 10 20:05:17 2016
Proceed anyway? (y,n) y
Creating filesystem with 3331328 4k blocks and 833952 inodes
Filesystem UUID: eb71decc-7380-4516-b542-4dd01bd1bb13
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (32768 blocks): done
Writing superblocks and filesystem accounting information: done   

/dev/sdc5 mounted ...
verify image ...
extracting image to /dev/sdc5 ...
finalizing storage ...

real	1m47.558s
user	0m0.000s
sys	0m0.020s
done!
```

The whole process took around 2 minutes! Most time spent in
the sync process.

## Login ##

```sh
ssh alarm@PI_IP #password: alarm
```


### Upgrade ###
```sh
su - root #password: root
pacman -Syuu
```

### Modify salt config ###

### Bootstrap Salt ###

```sh

# install the keyring
pacman -S archlinux-keyring

# update keyring
pacman-key --init && pacman-key --populate archlinux

# salt bootstrap
curl -L https://bootstrap.saltstack.com | sh
```

### Run the highstate ###

This command will apply the saltstack formula

```sh
salt-call --master=YOUR_MASTER --id=YOUR_MINION_ID state.highstate
```

## Cleanup ##

```sh
sync
reboot
```
